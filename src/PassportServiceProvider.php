<?php

namespace GlobalGarner\Passport;

use Illuminate\Support\ServiceProvider;


class PassportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

        config(['auth.guards.api.driver' => 'passport']);


        $this->mergeConfigFrom(
            __DIR__.'/config/database.php', 'database.connections'
        );

    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();

        $loader->alias('Laravel\Passport\AuthCode', AuthCode::class);
        $loader->alias('Laravel\Passport\Client', Client::class);
        $loader->alias('Laravel\Passport\PersonalAccessClient', PersonalAccessClient::class);
        $loader->alias('Laravel\Passport\Token', Token::class);
        $loader->alias('App\User', User::class);
    }
}
