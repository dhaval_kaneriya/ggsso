<?php

return [


    'passport' => [
        'driver' => 'mysql',
        'host' => env('DB_HOST_PASSPORT', '127.0.0.1'),
        'port' => env('DB_PORT_PASSPORT', '3306'),
        'database' => env('DB_DATABASE_PASSPORT', 'passport'),
        'username' => env('DB_USERNAME_PASSPORT', 'root'),
        'password' => env('DB_PASSWORD_PASSPORT', 'toor'),
        'unix_socket' => env('DB_SOCKET', ''),
        'charset' => 'utf8mb4',
        'collation' => 'utf8mb4_unicode_ci',
        'prefix' => '',
        'strict' => true,
        'engine' => null,
    ],


]

?>